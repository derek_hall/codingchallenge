// Derek Hall 
// Scientific Games Coding Challenge Submission
//
// Problem 1:
// Description
//		Given a list of people with their birth and end years(all between 1900 and 2000), find the year with the most number of people alive.
// Code
//		Solve using a language of your choice and dataset of your own creation.
// Submission
//		Please upload your code, dataset, and example of the programís output to Bit Bucket or Github. Please include any graphs or charts created by your program.

#include <iostream>
#include <assert.h>

struct Person
{
	// name/id (omitted since its not required to solve this problem)

	// year of birth
	int mBirthYear;
	
	// end year
	int mEndYear;
};

struct Result
{
	int mYear;
	int mNumOfLivingPeople;
};

Result FindYearOfMostLivingPeople(Person inputData[], int arrayLength)
{
	// the minimum allowed year value
	const int MIN_YEAR = 1900;
	// the maximum allowed year value
	const int MAX_YEAR = 2000;
	// the year range 1900 to 2000 = 101 years since we include the years 1900 and 2000
	const int YEAR_RANGE = MAX_YEAR - MIN_YEAR + 1;

	// this array stores the count of the number of people that are alive for each year
	int yearTable[YEAR_RANGE] = {};

	// fill the year table with data
	for (int listIdx = 0; listIdx < arrayLength; ++listIdx)
	{
		// convert the birth year and end years to indexes that are used
		// to index the yearTable
		const int & birthYear = inputData[listIdx].mBirthYear;
		assert(birthYear >= MIN_YEAR && birthYear <= MAX_YEAR);
		const int & endYear = inputData[listIdx].mEndYear;
		assert(endYear >= MIN_YEAR && endYear <= MAX_YEAR);
		assert(endYear >= birthYear);

		const int birthYearIdx = birthYear - MIN_YEAR;
		const int endYearIdx = endYear - MIN_YEAR;

		for (int yearTblIdx = birthYearIdx; yearTblIdx <= endYearIdx; ++yearTblIdx)
		{
			++yearTable[yearTblIdx];
		}
	}

	// this code assumes that we want the first occurence of the year with the 
	// most number of people alive. It would need to be modified to return multiple
	// years if there was a requirement to return all years that the most number of 
	// people were alive.
	int idxOfMaxLiving = 0;
	int maxNumLiving = 0;
	for (int yearTblIdx = 0; yearTblIdx < YEAR_RANGE; ++yearTblIdx)
	{
		if (yearTable[yearTblIdx] > maxNumLiving)
		{
			idxOfMaxLiving = yearTblIdx;
			maxNumLiving = yearTable[yearTblIdx];
		}
	}

	const int yearResult = idxOfMaxLiving + MIN_YEAR;

	Result result = { yearResult, yearTable[idxOfMaxLiving] };
	return result;
}


void main()
{
	// the input data array is written assuming we have a relatively small input data set that can fit in to contiguous memory locations.
	// For a larger data set that is too big to fit into contigous memory locations I would consider replacing the array with a linked list
	// based data structure so that the data doesn't need to fit into contiguous memory locations.
	
	
	// below I've included several test cases to verify that the FindYearOfMostLivingPeople funtion is working correctly
	//---------------------------------
	{
		Person inputData[] =
		{
			{1900, 1900},
		};

		const int LIST_SIZE = sizeof(inputData) / sizeof(inputData[0]);
		Result result = FindYearOfMostLivingPeople(inputData, LIST_SIZE);
		assert(result.mNumOfLivingPeople == 1);
		assert(result.mYear == 1900);
		std::cout << "The year " << result.mYear << " has the most number of people (" << result.mNumOfLivingPeople << ") alive" << std::endl;
	}
	// ---------------------------------
	{
		Person inputData[] =
		{
			{ 2000, 2000 },
		};

		// calculate the size of the input data set
		const int LIST_SIZE = sizeof(inputData) / sizeof(inputData[0]);
		Result result = FindYearOfMostLivingPeople(inputData, LIST_SIZE);
		assert(result.mNumOfLivingPeople == 1);
		assert(result.mYear == 2000);
		std::cout << "The year " << result.mYear << " has the most number of people (" << result.mNumOfLivingPeople << ") alive" << std::endl;
	}
	// ---------------------------------
	{
		Person inputData[] =
		{
			{ 1900, 2000 },
			{ 1950, 2000 },
		};

		// calculate the size of the input data set
		const int LIST_SIZE = sizeof(inputData) / sizeof(inputData[0]);
		Result result = FindYearOfMostLivingPeople(inputData, LIST_SIZE);
		assert(result.mNumOfLivingPeople == 2);
		assert(result.mYear == 1950);
		std::cout << "The year " << result.mYear << " has the most number of people (" << result.mNumOfLivingPeople << ") alive" << std::endl;
	}
	// ---------------------------------
	{
		Person inputData[] =
		{
			{ 1900, 1945 },
			{ 1910, 1960 },
			{ 1950, 1960 },
			{ 1941, 1999 },
			{ 1960, 1978 },
		};

		// calculate the size of the input data set
		const int LIST_SIZE = sizeof(inputData) / sizeof(inputData[0]);
		Result result = FindYearOfMostLivingPeople(inputData, LIST_SIZE);
		assert(result.mNumOfLivingPeople == 4);
		assert(result.mYear == 1960);
		std::cout << "The year " << result.mYear << " has the most number of people (" << result.mNumOfLivingPeople << ") alive" << std::endl;
	}
	// ---------------------------------
	{
		Person inputData[] =
		{
			{ 1900, 1910 },
			{ 1930, 1940 },
			{ 1950, 1960 },
			{ 1970, 1980 },
			{ 1990, 2000 },
		};

		// calculate the size of the input data set
		const int LIST_SIZE = sizeof(inputData) / sizeof(inputData[0]);
		Result result = FindYearOfMostLivingPeople(inputData, LIST_SIZE);
		assert(result.mNumOfLivingPeople == 1);
		assert(result.mYear == 1900);
		std::cout << "The year " << result.mYear << " has the most number of people (" << result.mNumOfLivingPeople << ") alive" << std::endl;
	}
	std::cout << "Press the enter key to quit";
	
	getchar();
}